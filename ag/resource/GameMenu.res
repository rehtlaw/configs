"GameMenu"
{
	"1"
	{
		"label" "RESUME FRAGGING"
		"command" "ResumeGame"
		"OnlyInGame" "1"
	}
	"2"
	{
		"label" "DISCONNECT"
		"command" "Disconnect"
		"OnlyInGame" "1"
	}
	"3"
	{
		"label" ""
		"command" ""
		"OnlyInGame" "1"
	}
	"7"
	{
		"label" ""
		"command" ""
		"notmulti" "1"
	}
	"8"
	{
		"label" "FIND SERVER"
		"command" "OpenServerBrowser"
	}
	"9"
	{
		"label" "CREATE SERVER"
		"command" "OpenCreateMultiplayerGameDialog"
	}
//	"10"
//	{
//		"name" "LoadDemo"
//		"label" "Play Demo"
//		"command" "OpenLoadDemoDialog"
//	}
	"11"
	{
		"label" ""
		"command" ""
	}
	"13"
	{
		"label" "OPTIONS"
		"command" "OpenOptionsDialog"
	}
	"14"
	{
		"label" "QUIT"
		"command" "Quit"
	}
}